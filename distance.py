'''
Description: sharp 2Y0A710K distance ranger.(Range from 1m to 5.5m.)
This code prints out data of valid range.
However, if we put ranger very close to objects, we will have completely wrong results.
The error is inevitable because of electric character of this device. 
So the only way of correct use is to make sure the distance of objects  not close to the ranger. (1m to 5.5m.)
Another tip: it is not very precise. So it is fit for detection,but not for measurement.       

Tested by Michael from DFrobot 
2012/2/13*/

/*Principle of this ranger: (See details in datasheet.)
Voltage is linear to the inverse of distance. (Distance range is 1m to 5.5m)
Two reference points:
Analog Voltage(V)   Digital value(0-1023)    Distance(cm)
  2.5                512                     100
  1.4                286                     500
Then, we have a linear equation of digital value and distance.
(512-sensorValue)/(1/100-1/distance)=(512-286)/(0.01-0.002) 
=> distance = 28250 / (sensorValue - 229.5);
'''

import sys

distance = int(sys.argv[1]);
sensorValue = 28250 / distance + 229.5;
print(sensorValue)
