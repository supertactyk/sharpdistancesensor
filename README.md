# Sharp GP2Y0A710K Distance Sensor (100-550cm)

- This code makes LED blinks depending on the subject sharp distance range.
- We are using the same electronic schematic as Arbreole firstPOC project: 
- https://bitbucket.org/supertactyk/firstpoc/src/d6419c34e25da558a4fe73bc81e850db4b110374/Arduino/setup_hardware_arduino.md 

## Information
- [Sharp Datasheet](https://cdn-shop.adafruit.com/datasheets/gp2y0a710k.pdf)
- [Getting Started with the Sharp sensor](https://www.dfrobot.com/wiki/index.php/Sharp_GP2Y0A710K_Distance_Sensor_(100-550cm)_(SKU:sen0085))

## Instructions of use
- The Arduino code is in the file [sharp_LED/sharp_LED.ino](sharp_LED/sharp_LED.ino)
- Upload the code to the Arduino the same way as the [firstPOC project](https://bitbucket.org/supertactyk/firstpoc/src/d6419c34e25da558a4fe73bc81e850db4b110374/Arduino/setup_hardware_arduino.md)

See the python script [distance.py](distance.py) if you want to calculate the sensorValue corresponding to an input distance
