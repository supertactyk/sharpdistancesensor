/*Description: sharp 2Y0A710K distance ranger.(Range from 1m to 5.5m.)
This code makes LED blinks depending on the subject sharp distance range.
We are using the same electronic schematic as Arbreole firstPOC project: 
https://bitbucket.org/supertactyk/firstpoc/src/d6419c34e25da558a4fe73bc81e850db4b110374/Arduino/setup_hardware_arduino.md 

Tested by Michael from DFrobot, modified by Yannick from Tactyk
2012/2/13 - 2017/12/1*/

/*Principle of this ranger: (See details in datasheet.)
Voltage is linear to the inverse of distance. (Distance range is 1m to 5.5m)
Two reference points:
Analog Voltage(V)   Digital value(0-1023)    Distance(cm)
  2.5                512                     100
  1.4                286                     500
Then, we have a linear equation of digital value and distance.
(512-sensorValue)/(1/100-1/distance)=(512-286)/(0.01-0.002) 
=> distance=28250/(sensorValue-229.5);
=> sensorValue=28250/d + 229.5;
See the python script distance.py if you want to calculate the sensorValue corresponding to an input distance

NB: if we put ranger very close to objects, we will have completely wrong results.
The error is inevitable because of electric character of this device. 
So the only way of correct use is to make sure the distance of objects not close to the ranger. (1m to 5.5m.)
Another tip: it is not very precise. So it is fit for detection,but not for measurement.      
*/

const int ledPin1 = 13;
const int ledPin2 = 9;
const int ledPin3 = 8;
const int sensorPin = 0;

void setup() {
  pinMode(ledPin1, OUTPUT);
  pinMode(ledPin2, OUTPUT);
  pinMode(ledPin3, OUTPUT);
  Serial.begin(9600);
}

int sensorValue;
int distance;

void loop() {
  sensorValue = analogRead(sensorPin);   

  if (sensorValue >= 386 && sensorValue <= 512) {
    printSerial(sensorValue);
    if (sensorValue >= 456) { // Corresponding distance range from 1m to 1m25
      lightOnly(1);
    } else if (sensorValue >= 418 && sensorValue <= 455) { // Corresponding distance range from 1m25 to 1m50
      lightOnly(2);
    } else { // Corresponding distance range from 1m50 to 1m80
      lightOnly(3);
    }
  } else {
    lightOnly(0);
  }
  delay(200);                  
}

void printSerial(int sensorValue){
  distance = 28250 / (sensorValue-229.5);
  Serial.print("The sensorValue is: ");
  Serial.print(sensorValue);
  Serial.print("; ");
  Serial.print("The distance is: ");
  Serial.print(distance);
  Serial.println("cm");
}

void lightOnly(int n){
  switch(n)
  {
    case 0:
    digitalWrite(ledPin1, LOW);
    digitalWrite(ledPin2, LOW);
    digitalWrite(ledPin3, LOW);
    break;
    case 1:
    digitalWrite(ledPin2, LOW);
    digitalWrite(ledPin3, LOW);
    digitalWrite(ledPin1, HIGH);
    break;
      case 2:
    digitalWrite(ledPin1, LOW);
    digitalWrite(ledPin3, LOW);
    digitalWrite(ledPin2, HIGH);
    break;
      case 3:
    digitalWrite(ledPin1, LOW);
    digitalWrite(ledPin2, LOW);
    digitalWrite(ledPin3, HIGH);
    break;
  }
}
